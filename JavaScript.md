# __JavaScript__
## __Basic data types and data structures__

 * __Data types__ are the classifications we give to the different kinds of data that we use in programming. 

* In __JavaScript__ there are 7 __primitive data types__ and 2 __structural data types__.

* To check the data type in JavaScript we use the typeof operator.
  ```javascript
  console.log(typeof "hello")  //prints string
  console.log(typeof 4)        //prints number
  ```
---
## __Primitive data types__
### __Boolean__

   * Boolean represents a __logical entity__ and can have two values i.e. __true__ and __false__. 

     ```javascript
     let nameFieldCheck = true;

     var isBusy = false;

     if(true){
        console.log(true)
     }
     else{
        console.log(false)
     };
      ```

### __Number__
   * The number type represents both the __integer__ and the __floating point__ numbers.
   
   * Besides regular numbers, there are so-called “special numeric   values” which also belong to this data type: __Infinity__, __-Infinity__ and __NaN__.

     ```javascript
     let number = 123;

     let floatingNumber = 12.345; 
     ```
### __BigInt__
   * In JavaScript, the “number” type cannot represent integer values larger than (253-1) (that’s 9007199254740991), or less than -(253-1) for negatives. It’s a technical limitation caused by their internal representation.

   * For most purposes that’s quite enough, but sometimes we need really big numbers, e.g. for cryptography or microsecond-precision timestamps.

   * A BigInt value is created by appending __n__ to the end of an integer.

     ```javascript
     const bigInt = 1234567890123456789012345678901234567890n;
     ```
### __String__
   * Anything between single or double __quotes__ is a string in
  JavaScript.
     ```javascript
     let string1 = 'Hello World';

     const string2 = "I like JavaScript";

     const numberStr = "123";
     ```
### __Null__
   * Null forms a separate type of its own which contains only the  null value.
     ```javascript
     let age = null;
     ```
### __Undefined__
   * A variable that has not been assigned any value has the value undefined.
     ```javascript
     let age;
     alert(age); // alert message undefined
     ```
### __Symbol__
   * A symbol is a __unique__ and __immutable__ primitive value and may be used as the key of an object property.
---
## __Structural data types__

### __Objects__
   * Objects are the collection of data in the form of __{key:value}__ pair, where keys are more like indentifiers to specific values.

   * The keys are either strings or unique symbol data type.

   * The values can be of any data type.
  
     ```javascript
     let person1 = {name:"akshay",age:25,profession:"engineer"};
     console.log(person1["name"]);
     ```

### __Functions__
   * In any programming language the general purpose of using functions is to avoid the __repeated__ coding.

   * A function in JavaScript is similar to a procedure set of statements that performs a task or calculates a value.

   * A function must be __defined__ in JavaScript before calling it.

     ```javascript
     function add(a,b){             //declaring the function   
         let sum = a+b
         return sum                //function return a value 
     }
     add(4,5);                    //calling the function

     const multiply = (a,b)=>{      //arrow function
         let product = a*b;
         return product;
     }
     multiply(3,5);
     ```
---
  
## __Reference__ 

* [MDN Web Docs](https://developer.mozilla.org/en-US/ "MDN Web Docs")
* [The Modern JavaScript Tutorial](https://javascript.info/ "The Modern JavaScript Tutorial")
* [Codecademy](https://www.codecademy.com/learn "Codecademy")

